document.addEventListener("mouseover", myFunction);

let defs = [ 
            //Bash
            ['Bash', "<h4>1. Základy</h4>" +
                     "<p>Podmienky(-lt, -ge, ...), manuál:</p>" +
                     "<p class=\"sourcecode\">man test</p>" +
                     "<h4>2. Podmienky</h4>" +
                     "Otestuje či je premenná rovná 1:" +
                     "<p class=\"sourcecode\"><span class=\"comment\">#!/bin/bash</span><br>" +
                     "A=1 <span class=\"comment\">#deklarácia premennej</span><br>" +
                     "if [ $A -eq 1 ]; then echo OK; else echo not OK; fi" +
                     "</p>" +
                     "<h4>3. Funkcie</h4>" +
                     "Deklarácia a volanie funkcie:" +
                     "<p class=\"sourcecode\">status() { <span class=\"comment\">#deklarácia funkcie</span><br>" +
                     "&nbsp;&nbsp;&nbsp;&nbsp;echo Function called!<br>" +
                     "}<br>" +
                     "<br>" +
                     "status <span class=\"comment\">#volanie funkcie</span><br>" +
                     "</p>"
            ], //end: Bash
            //Package manager
            [ 'PM', "<p>Pohľadá nainštalovanú aplikáciu/knižnicu:</p>" +
                    "<p class=\"sourcecode\">ldconfig -p | grep python</p> " +
                    "<p class=\"sourcecode\">apt-file search libQtSvg.so.4</p> " +
                    "<p class=\"sourcecode\">yum search microhttpd</p>" +
                    "<p class=\"sourcecode\">dpkg -S libQtSvg.so.4</p>" +
                    "<p class=\"sourcecode\">sudo pacman -Fy && pacman -Fx libalsa*</p>" +
                    "<p>Zistí veľkosť package(nemusí byt nainštalovaný) z repozitára:</p>" +
                    "<p class=\"sourcecode\">apt show firefox | grep Size</p>"
            ], //end: PM
            [ 'FS', "<p>Kto lockuje/drží súbor:</p>" +
                    "<p class=\"sourcecode\">sudo lsof &lt;subor&gt;</p>" +
                    "<p>Mount remote file system:</p>" +
                    "<p class=\"sourcecode\">sudo mount -t cifs -o user=&lt;user&gt; //remote_path /mnt/mount_name/</p>" +
                    "<p>Počíta výskyt stringu v súbore:</p>" +
                    "<p class=\"sourcecode\">grep -o \"2017008\" content.xml | wc -w</p>" +
                    "<p>Hľadá text v súboroch v aktuálnom adresári a všetkých podadresároch:</p>" +
                    "<p class=\"sourcecode\">grep -rnw -e \"pattern\"" +
			 		"<p>Soft link:</p>" +
                    "<p class=\"sourcecode\">sudo ln -s /usr/share/doc/qt6/ qt <span class=\"comment\">#ln -S source link_name</span></p>" +
                    "<p>Synchronizácia(kopírovanie) adresárov:</p>" +
                    "<p class=\"sourcecode\">rsync -avLP src dst</p>" +
                    "<p>Zápis ISO súboru na USB(bootable):</p>" +
                    "<p class=\"sourcecode\">sudo dd if=name.iso of=/dev/sdx bs=4M</p>" +
                    "<p>Voľné miesto v aktuálnom adresári:</p>" +
                    "<p class=\"sourcecode\">df -Ph . | tail -1 | awk '{print $4}'</p>"                    
            ], //end: File system
			[ 'HwAndKernel',
			  "<p>Informácia o hardware:</p>" +
			  "<p class=\"sourcecode\">hwinfo</p>" +
        "<p class=\"sourcecode\">sudo inxi -m <span class=\"comment\">#informácia o pamäťových moduloch: typ, rýchlosť, veľkosť v GB</span></p>" +
			  "<p class=\"sourcecode\">inxi -n <span class=\"comment\">#informácia o sieťových adaptéroch(network drivers)</span></p>" +
			  "<p class=\"sourcecode\">inxi -r <span class=\"comment\">#informácia o repositároch</span></p>" +
        "<p class=\"sourcecode\">inxi -Fz <span class=\"comment\">#komplet informácia o zariadení, aj kernel driver, teploty, verzie...</span></p>" +
			  "<p>Výstup(log) z kernel ring:" +
			  "<p class=\"sourcecode\">sudo dmesg</p>" +
        "<p>Vypíše hardware parametre aktuálneho PC(pamäť, BIOS, ...):" +
			  "<p class=\"sourcecode\">sudo lshw -C memory</p>"
			], //end: Hardware and Kernel
            [ 'TCPIP', "<p>Otvorené socket-y pre nejaký proces:</p>" +
              "<p class=\"sourcecode\">sudo netstat -tnpa | grep firefox*</p>" +
              "<p>Monitor sieťového prenosu:</p>" +
              "<p class=\"sourcecode\">sudo tcpdump -i any port 443</p>" +
			  "<p class=\"sourcecode\">sudo tcpdump host 192.168.0.1</p>" +
              "<p>Počúva na localhost, porte 443. Všetko čo príde vypíše do súboru '/tmp/test.log':" +
              "<p class=\"sourcecode\">sudo socat -u TCP4-LISTEN:443,reuseaddr,fork OPEN:/tmp/test.log,creat,append</p>" + //https://www.redhat.com/sysadmin/getting-started-socat
			  "<p>IPTABLES príklady - Linux firewall:" +
			  "<p class=\"sourcecode\">iptables -A INPUT -s xxx.xxx.xxx.xxx -j DROP <span class=\"comment\">#block an IP address</span>"
            ], //end: TCP/IP
            [ 'Git',
              "<p>Nájde commit podľa zadaného stringu:</p>" +
              "<p class=\"sourcecode\">git log -S hľadaný_string</p>" +
              "<p class=\"sourcecode\">git log --pretty=format:\"%h - %an, %ar : %s\" | grep hľadaný_string</p>" +
              "<p>Nájde commity po nejakom dátume:</p>" +
              "<p class=\"sourcecode\">git log --after=\"2020-04-01 00:00\"</p>" +
              "<p>Definícia aliasu:</p>" +
              "<p class=\"sourcecode\">git config --global alias.st status<br>git config --global alias.pr \"pull -r\"</p>" +
              "<p>Revert local commit - zmaže lokálny commit:</p>" +
              "<p class=\"sourcecode\">git reset HEAD~1</p>" +
              "<p>Premapovanie git protokolu na https(ak sa neda, lebo connection denied):</p>" +
              "<p class=\"sourcecode\">git config --global url.\"https://\".insteadOf git://</p>" +
              "<p>Najde najnovsi/posledny tag podla prefixu(priklad pre verziu 15):" +
              "<p class=\"sourcecode\">git tag  | grep -E '15\\.' | sort -V | tail -1</p>"
            ], //end: Git
            [ 'Docker',
              "<p>Základná inštalácia a run(príklad pre nginx):</p>" +
              "<p class=\"sourcecode\">sudo snap install docker</p>" +
              "<p class=\"sourcecode\">sudo docker pull nginx</p>" +
              "<p class=\"sourcecode\">sudo docker run --name ngx-docker -p 1234:80 -d nginx<span class=\"comment\"> #vytvorenie kontajnera pre nginx, bude bežať na localhost:1234</span></p>" +
              "<p class=\"sourcecode\">sudo docker stop ngx-docker<span class=\"comment\"> # znovu nastartovanie: sudo docker start ngx-docker</span></p>" +
              "<p>Zoznam bežiacich kontajnerov:</p>"+
              "<p class=\"sourcecode\">sudo docker ps</p>" +
              "<p>Zoznam všetkých kontajnerov:</p>"+
              "<p class=\"sourcecode\">sudo docker ps -a</p>" +
              "<p>Remote shell do kontajneru:</p>"+
              "<p class=\"sourcecode\">docker exec -t -i <CONTAINER_ID> /bin/bash</p>"
            ], //end: Docker
			[ 'OpenWrt',
			  "<p>Upgrade všetkých packages(LEN V AKÚTNYCH PRÍPADOCH!):</p>" +
                "<p class=\"sourcecode\">opkg list-upgradable | cut -f 1 -d ' ' | xargs -r opkg upgrade</p>" +
              "<p>Zmena opkg konfigurácie balíčka, <a href=\"https://openwrt.org/docs/guide-user/installation/sysupgrade.cli#manual_config_diff\">link</a>:</p>" +
                "<p class=\"sourcecode\">opkg install diffutils</p>" +
                "<p class=\"sourcecode\">find /etc -name *-opkg<span class=\"comment\"> # locate all -opkg files</span></p>" +
                "<p class=\"sourcecode\">diff /etc/config/snmpd /etc/config/snmpd-opkg<span class=\"comment\"> # Príklad pre 'snmpd' package</p>" +
              "<p>Ako uvoľniť miesto v RAM/storage:</p>" +
                "<p class=\"sourcecode\">free<span class=\"comment\"> # Zobraziť informáciu o RAM</span></p>" +
                "<p class=\"sourcecode\">rm -r /tmp/opkg-lists/</p>" +
                "<p class=\"sourcecode\">sync && echo 3 > /proc/sys/vm/drop_caches</p>"
			], //end: OpenWrt
      [ 'RegExp',
          "<p>Nájde stringy \"&ltKey>aip\" za ktorým nenasleduje znak '.':</p>" +
          "<p class=\"sourcecode\">&ltKey>aip[^\.]</p>"
      ], //end: Regexp
      [ 'GRUB',
          "<p><u>GRUB</u> - GNU GRand Unified Bootloader, <a href=\"https://en.wikipedia.org/wiki/GNU_GRUB\" target=\"_blank\">link</a></p>" +
          "<p><u>NVRAM</u> - non-volatile random-access memory</p>" +
          "<p><u>UEFI</u> - Unified Extensible Firmware Interface, <a href=\"https://en.wikipedia.org/wiki/UEFI\" target=\"_blank\">link</a></p>" +
          "<br>" +
          "<p><i>1. Linux shell utilities:</i></p>" +
          "<div style=\"margin: 15px;\">" +
          "<p><i><strong>efibootmgr</i></strong> - is a userspace application used to modify the UEFI Boot Manager. Popisuje \"boot order\" postupnosť.</p>" +
          "<p><i><strong>os-prober</i></strong> - utilita na detekciu všetkých operačných systémov na všetkých diskoch." +
          "V GRUB ju treba povoliť(súbor <i>/etc/default/grub</i>):" +
            "<p class=\"sourcecode\">GRUB_DISABLE_OS_PROBER=false</p>" +
          "<p><i><strong>grub-install</strong></i> - nainštaluje GRUB na DISK(nikdy sa toto nerobí len pre partíciu):" +
          "<p class=\"sourcecode\">grub-install -v /dev/sda " +
            "<img onclick=\"copyTextToClipboard('grub-install -v /dev/sda')\" style=\"width: 16px;height:16px;\" src=\"img/copy_icon.png\" alt=\"Copy to Clipboard\"></p>" + //copy icon
          "<p><i><strong>update-grub</i></strong> - aktualizácia grub konfigurácie v súbore <i>/boot/grub/grub.cfg</i> so všetkými boot partitions ktoré nájde na všetkých pripojených diskoch a partíciách.</p>" +
          "</div>" +
          "<br>" +
          "<p><i>2. Príkazy v konzole GRUB:</i></p>" +
          "<div style=\"margin: 15px;\">" +
          "<p><i><strong>ls</i></strong> - zobrazí zoznam všetkých pripojených jednotiek(partitions)</p>" +
          "<p><i><strong>configfile</i></strong> - načítanie konfiguračného súboru rovno z GRUB konzoly, príklad:" +
            "<p class=\"sourcecode\">grub> configfile (hd0,msdos2)/boot/grub/grub.cfg " +
          "<p>Príklad postupnosti príkazov z konzoly GRUB na nabootovanie partície:</p>" +
          "<p class=\"sourcecode\">" +
            "grub> linux (hd0,gpt2)/vmlinuz root=/dev/sda2<br>" +
            "grub> initrd (hd0,gpt2)/initrd.img<br>grub> boot<br></p>" +
          "</div>" +
          "<br>" +
          "<br>" +
          "<small>* Zdroj:<br>" + 
          "https://www.root.cz/clanky/jak-funguje-a-jak-nastavit-bootovani-aneb-grub-1-2-mbr-uefi/</small>"
      ], //end: GRUB
      [ 'Firejail',
          "<p>Disable internet pre program:</p>" +
          "<p class=\"sourcecode\">firejail --net=none <i>program</i>" +
          "<img onclick=\"copyTextToClipboard('firejail --net=none firefox')\" style=\"width: 16px;height:16px;\" src=\"img/copy_icon.png\" alt=\"Copy to Clipboard\">" + //copy button
          "</p>"
      ] //end: Firejail
  ];
           
function search() {
    //search query and set paragraphs found
    var input = document.getElementById("mySearch");
    var filter = input.value.toUpperCase();
    if( filter.length < 3 ) {
        return;
    }

    document.getElementById("definition").innerHTML = "";
    for (var defsCounter = 0; defsCounter < defs.length; defsCounter++) {
        var tmp = document.createElement('div');
        tmp.innerHTML = defs[defsCounter][1];

        var allParagraphs = tmp.getElementsByTagName("p");
        for (var i = 0; i < allParagraphs.length; i++) {
            if (allParagraphs[i].textContent.toUpperCase().indexOf(filter) > -1) {
                document.getElementById("definition").innerHTML += allParagraphs[i].outerHTML;
            }
        }
    }
}

let selectedElement = null;
function myFunction(fParam) {
    
  if( selectedElement != null ) {
    return;
  }
  
  // Declare variables
  var input, filter, ul, li, a, i;
  input = document.getElementById("mySearch");
  filter = input.value.toUpperCase();
  ul = document.getElementById("myMenu");
  li = ul.getElementsByTagName("li");

  if( fParam == "AWK" ) {
    document.getElementById("definition").innerHTML =
    "<h4>1. Parsovanie stringov:</h4>" +
    "<p>Vypíše reťazec za znakom '=', výstup bude 'retazec2':</p><p class=\"sourcecode\">echo retazec1=retazec2 | awk -F'=' '{print $2}'</p>"

    return;
  }

  if( fParam == "Bash" ) {
    document.getElementById("definition").innerHTML = defs[0][1];

    return;
  }

  if( fParam == "PM" ) {
    document.getElementById("definition").innerHTML = defs[1][1];
  }

  if( fParam == "FS" ) {
    document.getElementById("definition").innerHTML = defs[2][1];
  }
	
  if( fParam == "HwAndKernel" ) {
    document.getElementById("definition").innerHTML = defs[3][1];
  }

  if( fParam == "TCPIP" ) {
    document.getElementById("definition").innerHTML = defs[4][1];
    return;
  }

  if( fParam == "Git" ) {
    document.getElementById("definition").innerHTML = defs[5][1];
    return;
  }

  if( fParam == "Docker" ) {
    document.getElementById("definition").innerHTML = defs[6][1];
    return;
  }

  if( fParam == "OpenWrt" ) {
    document.getElementById("definition").innerHTML = defs[7][1];
    return;
  }

  if( fParam == "RegExp" ) {
    document.getElementById("definition").innerHTML = defs[8][1];
    return;
  }

  if( fParam == "GRUB" ) {
    document.getElementById("definition").innerHTML = defs[9][1];
    return;
  }

  if( fParam == "Firejail" ) {
    document.getElementById("definition").innerHTML = defs[10][1];
    return;
  }
}

function itemMenuClicked(elemId)
{
    if( document.getElementById(elemId) == selectedElement ) {
        selectedElement.style = "background-color:#bbb";
        selectedElement = null;
        return;
    }
    
    if( selectedElement != null ) {
        selectedElement.style = "background-color:#bbb";
    }

    selectedElement = document.getElementById(elemId);
    selectedElement.style = "background-color:#faf2eb";
}

function readSingleFile(e) {
  var file = e.target.files[0];
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    displayContents(contents);
  };
  reader.readAsText(file);
}

function displayContents(contents) {
  var element = document.getElementById('definition');
  element.textContent = contents;
}
