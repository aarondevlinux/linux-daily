https://github.com/rasbt/python_reference/tree/master/tutorials/markdown_syntax_highlighting

### Activate Python env

	python3 -m venv .venv
	source venv/bin/activate

### Requirements
	pip install markdown Pygments bs4

Python markdown docu: https://python-markdown.github.io/ <br>
<br>
Pygments languages: https://pygments.org/languages/ <br>
Pygments styles:    https://pygments.org/styles/