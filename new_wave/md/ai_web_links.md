## <ins>AI web links</ins>

URL    | Popis
------ | -------------
[web](https://codepal.ai/code-reviewer/cpp) | Code reviewer
[web](https://www.blackbox.ai/chat/08Ezrvn) | Blackbox AI(plus GPT, Gemini, Claude-Sonnet)
[web](https://zzzcode.ai/cplusplus/code-generator?id=223aa547-a2f9-4d69-be07-45e347697d35) | C++ code generator
[web](https://syntha.ai/reviewers/cpp) | C++ code reviewer
[web](https://workik.com/ai-code-explainer) | Code explainer