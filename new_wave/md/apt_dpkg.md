## <ins>apt/dpkg</ins>

### 1. Hľadanie

	:::bash
	dpkg -S libpython*

alebo(hľadá string v repozitároch)

	::bash
	apt-file search libpython

### 2. Základné použitie

* obnoví zoznamy balíčkov z repozitárov

		:::bash
		apt-get update

* ponúkne upgrade nainštalovaných balíčkov

		:::bash
		apt-get upgrade

* dělá upgrade nainstalovaných balíčků, ale nebojí se toho, když je kvůli tomu potřeba nainstalovat, nebo naopak odstranit, nějaký jiný balíček. Typický příklad je kernel.

		:::bash
		apt-get dist-upgrade

* zmaže súbory stiahnutých balíčkov a tým uvoľní miesto v `/var/cache/apt/archives`.
cit.: _Kdybyste pak nějaký balíček chtěli reinstalovat, tak se bude muset znova stáhnout, ale jak často takovou věc děláte?_

		:::bash
		apt-get clean

### 3. Info o balíčkoch

* skript na zoradenie nainštalovaných baličkoch podľa veľkosti

		:::bash
		for p in `dpkg-query -W --showformat='${Installed-Size;10}\t${Package}\n' | sort -k1,1n| tac | tr -s " " | cut -f 2`; do \
			if [ `apt-get -s purge $p | grep -E "^(Purg|Inst|Conf)" | wc -l` -eq 1 ]; then echo $p; fi \
		done

Zdroj: [abclinuxu.cz](https://www.abclinuxu.cz/blog/jenda/2020/12/jak-pouzivat-apt-a-par-postrehu-ze-spravy-debianu)
