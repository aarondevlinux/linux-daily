## <ins>AWK</ins>

### 1. Parsovanie stringov:
Vypíše reťazec za znakom '=', výstup bude 'retazec2':

    :::bash
    echo retazec1=retazec2 | awk -F'=' '{print $2}'