## <ins>Bash</ins>

### 1. Základy

Podmienky( `-lt`, `-ge`, ...), manuál:

	:::bash
    man test

### 2. Podmienky

	:::bash
	#!/bin/bash
	A=1
	#legacy style(používaný aj v starších shell-och)
	if [ $A -eq 1 ]; then echo OK; else echo not OK; fi

	#rozšírená verzia(s dvomi '[['), špecifická pre Bash a niektoré iné moderné shelly
	if [[ $A -eq 1 ]]; then echo OK; else echo not OK; fi
	retazec="8855"
	if [[ $retazec =~ ^[0-9]+$ ]]; then echo "Reťazec je číselný."; fi

### 3. Funkcie
Deklarácia a volanie funkcie:

	:::bash
	status() { #deklarácia funkcie
	    echo Function called!
	}

	status #volanie funkcie status()

### 4. Cykly

	:::bash
	for x in {1..5}; do
		echo $x
	done

### 5. Commands(Príkazy)

* spustenie viacerých príkazov v **separátnom shell(subshell)**, izolovanom od aktuálneho shell:

		:::bash
		( ls ~; whoami )

* spustenie toho istého, ale v **aktuálnom shell**:

		:::bash
		{ ls ~; whoami; }

* matematické operácie:

		:::bash
		result=$((5*4+1))
		echo $result

* volanie príkazu so zachytením výsledku:

		:::bash
		result=$(grep "configure" /var/log/dpkg.log)
		echo $result

### 6. Súbory
Check či súbor existuje(dôležité je dodržať medzeru za `if`):

	:::bash
	#!/usr/bin/bash

	SUBOR_CESTA=/usr/bin/bash2
	if [[ ! -f $SUBOR_CESTA ]]; then
		echo "Subor '${SUBOR_CESTA}' sa nenasiel!"
		exit 1
	fi

	printf "OK, subor '%s' mame!\n" $SUBOR_CESTA

### 7. Strings(Reťazce)
Nahradenie `.txt` -> `.bak`

	:::bash
	filename=nieco.txt
	echo ${filename%.txt}.bak

### 8. Rozširená expanzia parametrov

`B=w/x/y/z && echo ${B##*x/}` vráti `y/z`
<br><br>
`C=/opt/filename.tmp && echo ${C##*.}` vráti `tmp` - vhodné pri názvoch súborov napríklad
<br><br>
`B=w/x/y/z && echo ${B%y*}` vráti `w/x/`

Kontrolór správnosti skriptov: [shellcheck](https://www.shellcheck.net/)
