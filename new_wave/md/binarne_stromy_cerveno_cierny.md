## <ins>Binárne stromy - red-black tree</ins>

### 1. Charakteristika

Červeno-čierne stromy sú špeciálny typ vyváženého binárneho vyhľadávacieho stromu, ktorý zaisťuje, že operácie ako vkladanie, vymazávanie a vyhľadávanie sú efektívne, s časovou zložitostou O(log n). Tieto stromy sú navrhnuté tak, aby udržiavali vyváženú štruktúru, čím sa zabraňuje degenerácii do lineárnej štruktúry, ako je zoznam.

### Hlavné pravidlá červeno-čiernych stromov:

1. Každý uzol je buď **červený alebo čierny.**
2. Korene stromu sú **čierne.**
3. Každý list (NIL uzol) je **čierny.**
4. Ak je uzol **červený**, obe jeho deti musia byť **čierne** (žiadne dve červené uzly sa nesmú objaviť za sebou).
5. **Každá cesta z uzla k jeho potomkom NIL uzlom musí obsahovať rovnaký počet čiernych uzlov.** (Toto sa nazýva čierna výška.)

<img src="./static/red_black_tree.png" width="480" style="margin-left: 40px;">

### Výhody červeno-čiernych stromov:

- **Vyváženie:** Tieto pravidlá zabezpečujú, že dĺžka ciest v strome je udržiavaná na rozumných úrovniach, čo prispieva k efektívnosti operácií.
- **Rýchlosť:** Vkladanie a vymazávanie uzlov sú optimalizované, takže operácie môžu byť vykonávané rýchlo aj v prípade veľkých množín dát.

### Vkladanie a odstraňovanie:

- **Vkladanie:** Po vložení nového uzlu sa môže stať, že narušuje pravidlá červeno-čierneho stromu. V takých prípadoch sa vykonávajú rôzne rotácie a farebné úpravy, aby sa obnovila vyváženosť stromu.
- **Odstraňovanie:** Odstránenie uzla tiež môže narušiť vyváženosť, a preto sa vykonávajú úpravy a rotácie na obnovu pravidiel.

Červeno-čierne stromy sú veľmi užitočné v rôznych aplikáciách, kde je potrebné efektívne spravovať dynamické množiny dát, ako sú databázy a systémy správy pamäte.

Zdroj: [Džepeto](https://talkai.info/chat/)
[Geeksforgeeks](https://www.geeksforgeeks.org/red-black-tree-definition-meaning-in-dsa/)