## <ins>Binárne stromy - základné pojmy a teória</ins>

### 1. Terminológia

- ak z nejakého vrcholu A vedie hrana (šípka) do vrcholu B, tak vrchol A nazveme otcom (alebo predchodcom) a vrchol B synom

- ak sú dva vrcholy spojené hranou, hovoríme, že spolu `susedia`

- ak nejaký vrchol nemá žiadnych synov, hovoríme mu `list`, inak je to `vnútorný vrchol` stromu

- podstrom - nejaký vrchol považujeme za koreň a všímame si len jeho synov a ich synov atď.

- `cesta` - postupnosť vrcholov spojených hranami (len jedným smerom)

- dĺžka cesty = počet vrcholov na ceste-1 (t.j. počet hrán na ceste)

- `výška/hĺbka stromu` = dĺžka najdlhšej cesty od koreňa k listom (prázdny strom=?; len koreň=0; ...)

- `binárny strom` - každý vrchol má maximálne 2 synov

### 2. Prechody binárnym stromom

- `preorder` - uzol, ľavý podstrom, pravý podstrom

- `inorder` - ľavý podstrom, uzol, pravý podstrom

- `postorder` - ľavý podstrom, pravý podstrom, uzol

<img src="./static/binarny_strom_priklady_prechodov.png" width="480" style="margin-left: 40px;">

<br>
<br>
Zdroj: [FMFI](https://fmfi-uk.hq.sk/Informatika/Programovanie/prednasky/pl29.html)
[UPJS](https://paz1b.ics.upjs.sk/wp-content/uploads/sites/9/2024/04/prednaska4.pdf)