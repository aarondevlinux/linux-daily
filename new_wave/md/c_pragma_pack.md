## <ins>Pragma pack v C</ins>

#### Príklad:

	:::cpp
	#include <stdio.h>

	struct s_packed { //size: 13
		int i;
		char ch;
		double d;
	} __attribute__((packed));// Attribute informing compiler to pack all members

	struct s_not_packed { //size: 16
		double d;
		int i;
		char ch;
	};

	int main()
	{
		struct s_packed A;
		struct s_not_packed B;
		printf("Size of packed     structure is: %ld\n", sizeof(A));
		printf("Size of NOT packed structure is: %ld\n", sizeof(B));
		printf( "Struct members:\n - sizeof(int) = %ld\n - sizeof(char) = %ld\n - sizeof(double) = %ld\n",
				sizeof(int),
				sizeof(char),
				sizeof(double)
		);
	}
