## <ins>Quick sort v C</ins>

#### Príklad pre quick sort v C - zoradenie podľa abecedy od z po a(opačné poradie):

	:::cpp
	#include <stdlib.h>
	#include <string.h>
	#include <stdio.h>

	int compare(const void* a, const void* b) {
		return strcmp(*(const char**)b, *(const char**)a); //b vs. a => v opačnom poradí(od najväčšieho po najmenšie)
	}

	void sort_words(char *words[], int count)
	{
		qsort(words, count, sizeof(char*), compare);
	}

	int main()
	{
		char* words[] = { "cherry", "orange", "apple" };

		sort_words(words, 3);
		for (int i = 0; i < 3; i++)
			printf("%d. %s\n", i + 1, words[i]);
	}
