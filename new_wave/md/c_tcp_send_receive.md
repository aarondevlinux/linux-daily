## <ins>TCP send/receive v C</ins>

	:::cpp
	#include <stdio.h>
	#include <string.h>
	#include <unistd.h>
	#include <arpa/inet.h>

	#define PORT 1234
	#define BUFFER_SIZE 1024

	int main() {

		int sockfd;
		char buffer[BUFFER_SIZE];

		if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			perror("Socket creation error");
			return -1;
		}

		struct sockaddr_in serv_addr;
		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(PORT);

		if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) { //treba spustit lokalny server: socat -v tcp-l:1234,fork exec:'/bin/cat'
			perror("Invalid address/ Address not supported");
			return -1;
		}

		if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
			perror("Connection failed");
			return -1;
		}

		const char *message = "Hello, Server!";
		send(sockfd, message, strlen(message), 0);
		printf("Message sent: %s\n", message);

		int valread = read(sockfd, buffer, BUFFER_SIZE);
		if (valread < 0) {
			perror("Error reading from socket");
			return -1;
		}
		buffer[valread] = '\0';
		printf("Received %d bytes from server: '%s'\n", valread, buffer);

		close(sockfd);
		return 0;
	}
