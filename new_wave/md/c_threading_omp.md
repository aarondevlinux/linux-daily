## <ins>Fast linear search using OMP</ins>

#### Príklad pre linear search s použitím OMP:

	:::cpp
	#include <stdio.h>
	#include <omp.h>
	#include <time.h>

	#define N 999999 //pocet iteracii pre loop

	int main() {
		int a[N], sum = 0, sum_omp = 0;
		double time_spent, time_spent_omp;
		clock_t begin, end;

		// 0. Generovanie vstupneho pola
		for (int i = 0; i < N; i++)
			a[i] = 1;

		// 1. OMP (Open Multi-Processing)
		omp_set_num_threads(8); // Nastavenie poctu vlakien pre OMP
		begin = clock();
		#pragma omp parallel for reduction(+:sum_omp)
		for (int i = 0; i < N; ++i) sum_omp += a[i];
		end = clock();
		time_spent_omp = (double)(end - begin) / (double)CLOCKS_PER_SEC;

		// 2. Klasicky linear pristup
		begin = clock();
		for (int i = 0; i < N; ++i) sum += a[i];
		end = clock();
		time_spent = (double)(end - begin) / (double)CLOCKS_PER_SEC;

		// => Vysledok
		printf("Linear trvanie: %fs\n", time_spent);
		printf("OMP trvanie:    %fs\n", time_spent_omp);
		printf("Overenie vysledku: %s\n", (sum_omp == sum ? "OK" : "CHYBA"));
		printf("Rozdiel: %s\n", (time_spent_omp < time_spent ? "OMP wins" : "Linear wins"));

		return 0;
	}

build:

	:::bash
	gcc -g -lpthread -std=c18 -fopenmp -c threading.c -o obj/threading.c.o
	gcc -o main_executable_c -Wall -std=c18 -lpthread -fopenmp obj/threading.c.o
