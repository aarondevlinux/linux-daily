## <ins>CMake</ins>

### 1. Základný CMakeLists.txt

	:::cmake
	cmake_minimum_required(VERSION 3.10)

	set(CMAKE_CXX_COMPILER "arm-linux-gnueabihf-g++")
	set(CMAKE_CXX_STANDARD 17)
	add_compile_options(-Wno-psabi -pthread)

	project(NazovProjektu)

	set(CMAKE_CXX_STANDARD_REQUIRED True)

	add_executable(benchmarking main.cpp)

