## <ins>Codium - launch.json</ins>

### 1. Základný súbor launch.json

	:::json
	{
	"version": "0.2.0",
	"configurations": [
		{
			"name": "C/C++ Runner: Debug Session",
			"type": "lldb",
			"request": "launch",
			"args": [], //vstupne parametre pre app
			"stopOnEntry": false,
			"terminal" : "console",
			//"externalConsole": false,
			//"cwd": "${workspaceFolder}/",
			"program": "${workspaceFolder}/app_to_debug",
			"env" : {
				"LD_LIBRARY_PATH" : "/usr/local/lib/:/usr/lib64/"
			}
		}
	]
	}