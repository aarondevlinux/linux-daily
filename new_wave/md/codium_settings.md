## <ins>Codium - settings.json</ins>

### 1. Základný súbor settings.json

	:::json
	{
		"editor.detectIndentation": false,
		"editor.insertSpaces": false,
		"editor.tabSize": 4,
		"[json]": {
			"editor.tabSize": 2
		},

		"workbench.colorCustomizations": {
			"tab.activeBorder": "#ff0000",
			"tab.unfocusedActiveBorder": "#000000"
		}
	}