## <ins>coredump</ins>

#### Informácia o poslednom coredump:

	:::bash
	coredumpctl dump

#### Debug posledného core dumped procesu:

	:::bash
	coredumpctl debug
