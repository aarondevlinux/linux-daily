## <ins>curl</ins>

### 1. Príklady

* `GET`: Lists of filters used by ad blockers

		:::curl
		curl https://api.filterlists.com/lists

* `POST`:

		:::bash
		curl -X POST https://api.restful-api.dev/objects
		     -H 'Content-Type: application/json'
			 -d '{"name":"Apple MacBook Pro 16","data":{"year":2019,"price":2049.99,"CPU model":"Intel Core i9","Hard disk size":"1 TB","color":"silver"}}' 