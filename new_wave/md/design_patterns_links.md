## <ins>Design patterns web links</ins>

URL    | Popis
------ | -------------
[web](https://refactoring.guru/design-patterns) | Podrobné vysvetlenie na refactoring guru portáli
[web](https://msgprogramator.sk/java-navrhove-vzory/) | Krátke zhrnutie aj s popisom vzorov

<br>
Jednotlivé vzory najlepšie vysvetlené:

URL    | Názov           | Popis
------ | --------------- | -------------
[web](https://jirkasa.github.io/navrhove-vzory-v-js/mediator/) | Mediator | Behavioral design pattern that lets you reduce chaotic dependencies between objects. The pattern restricts direct communications between the objects and forces them to collaborate only via a mediator object.
[web](https://refactoring.guru/design-patterns/bridge) | Bridge | Structural design pattern that lets you split a large class or a set of closely related classes into two separate hierarchies—abstraction and implementation—which can be developed independently of each other.

<br>
<br>
Zdroj: [refactoring.guru](https://refactoring.guru/design-patterns/)
