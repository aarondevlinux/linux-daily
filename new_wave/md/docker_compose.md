## <ins>docker compose</ins>

### 1. Základný príklad(template) pre docker compose:

Dockerfile:

	:::docker
	FROM alpine:latest
	CMD sleep 999999999

docker-compose.yml:

	:::docker
	version: '3'
	services:
	  test-service:
	    image: image_name
		build:
		  context: .
		  dockerfile: Dockerfile
		container_name: container_name
    	ports:
    	  - 8000:8000
		volumes:
		  - /var/opt/host_filesystem/:/var/opt/container_filesystem/

Použitie:

	:::docker
	docker compose -f docker-compose.yml up -d --build
