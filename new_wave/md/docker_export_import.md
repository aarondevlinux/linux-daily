## <ins>Docker - export/import</ins>

export image

	:::docker
	docker save -o image.tar image_id

import image

	:::docker
	docker load -i image.tar

export kontajner

	:::docker
	docker export grafana | gzip > grafana.gz

import kontajner

	:::docker
	zcat grafana.gz | docker import - container_name