## <ins>Docker - konfigurácia</ins>

Konfigurácia kontajneru(súbor):

	:::bash
	/var/lib/docker/containers/<containerId>/config.v2.json

Konfigurácia kontajneru(command):

	::bash
	docker inspect <containerId>

Aká verzia OS beží v kontajneri:

	::bash
	docker exec container-name cat /etc/os-release
