## <ins>docker network</ins>

### 1. Základná analýza

	:::docker
	docker network ls

Detaily o už existujúcej sieti v docker:

	::docker
	docker network inspect <network_id>

### 2. Priradenie kontajneru do už existujúcej siete:

	::docker
	docker network connect <network_id> <container_id>

### 3. docker compose príklad:

	:::docker
	services:
		container_name:
			networks:
				- existing_network_name #priradenie do uz existujucej docker network 

	networks:
		existing_network_name:
			external: true