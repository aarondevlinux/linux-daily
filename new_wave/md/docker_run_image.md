## <ins>Docker - run image nginx</ins>

	:::bash
	docker pull nginx

Vytvorenie kontajnera pre nginx, bude bežať na `localhost:1234`

	:::docker
	docker run --name ngx-docker -p 1234:80 -d nginx

Stop container <!--potom znovu nastartovanie: `sudo docker start ngx-docker`-->

	:::docker
	docker stop ngx-docker