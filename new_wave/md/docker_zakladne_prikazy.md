## <ins>Docker - základné príkazy</ins>

Shell do kontajneru ako root:

	:::docker
	docker exec -it -u 0 7483afae60c6 bash

Zmazanie stopnutých kontajnerov, images, network - údržba nepotrebného:

	:::docker
	docker system prune

Copy lokálny súbor do kontaineru:

	:::docker
	docker cp foo.txt container_id:/foo.txt

Kde je lokálne uložený docker image súbor:

	:::docker
	docker inspect image_name | grep UpperDir

Povolenie localhost pre kontajner - potom bude prístup na host bežiace služby:

	:::docker
	docker run --net="host" 0291ea90a571

Docker daemon stop(pre SysV init system):

	:::docker
	sudo /etc/init.d/docker stop

Sledovanie logov z kontajneru(follow + len poslednych 50 riadkov):

	:::docker
	docker logs a29b56544e40 -n 50 -f
