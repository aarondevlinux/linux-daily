## <ins>firejail - blockings</ins>

Zákaz akejkoľvek sieťovej komunikácie(aj localhost) pre program:

	:::bash
	firejail --net=none firefox

Sandbox(--private) - cisty chroot(zmaze vsetko po shutdown):

	:::bash
	firejail --private firefox

Directory whitelist/blacklist, profily sú v `/etc/firejail`:

	:::bash
	whitelist ${DOWNLOADS}
	whitelist ${HOME}/.pki
	blacklist ${PATH}/curl

<br><br>
Zdroj: [man7.org](https://www.man7.org/linux/man-pages/man1/Firejail.1.html)
