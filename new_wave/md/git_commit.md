## <ins>git commit</ins>

### 1. commit spolu s "git identity"

	:::bash
	git -c "user.name=u" -c "user.email=1@2.com" commit -m "LeMessage"

### 2. commit revert

Revert lokálneho commit-u:

	:::git
	git reset HEAD~1

### 3. commit + log

Nájde commit podľa zadaného stringu:

	:::bash
	git log -S hľadaný_string

alebo:

	:::bash
	git log --pretty=format:"%h - %an, %ar : %s" | grep hľadaný_string