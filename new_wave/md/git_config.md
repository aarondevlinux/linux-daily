## <ins>git config</ins>

### 1. Základné alias-y

	:::bash
	git config --global alias.st status &&
	git config --global alias.pr "pull -r" &&
	git config --global alias.ch "checkout"

Premapovanie git protokolu na https(ak sa nedá, lebo connection denied):

	:::git
	git config --global url."https://".insteadOf git://