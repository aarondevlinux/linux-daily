## <ins>git merge</ins>

### 1. Manuálny merge branch-u "test" do main branch:

	:::git
	git checkout main
	git pull origin main
	git merge test
	git push origin main
