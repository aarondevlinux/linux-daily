## <ins>git mergetool</ins>

### 1. Príklad konfigurácie pre p4merge

Nasledovnú konfiguráciu treba pridať do `.gitconfig` súboru:

	:::git
	[diff]
		tool = p4merge
	[difftool]
		prompt = false
	[difftool "p4merge"]
		path = /opt/p4v-2017.3.1601999/bin/p4merge
	[merge]
		tool = p4merge
	[mergetool "p4merge"]
		path = /opt/p4v-2017.3.1601999/bin/p4merge
	[mergetool]
		keepBackup = false

### 2. Resolve conflict po 'stash pop' alebo 'pull -r'

	:::git
	git mergetool
