## <ins>grep - v súboroch</ins>

<br>
Hľadá text v súboroch v aktuálnom adresári a všetkých podadresároch:

    :::bash
    grep -rnw -e "pattern"

Počíta výskyt stringu v súbore:

    :::bash
    grep -o "2017008" content.xml | wc -w
