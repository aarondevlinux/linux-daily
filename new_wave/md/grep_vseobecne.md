## <ins>grep - všeobecne</ins>

Nájde stringy `aip` za ktorým **NE**nasleduje znak bodka `.`:

	:::grep
	aip[^.]

### Kvantifikátory

Quant. | Popis
------ | -------------
*      | < 0; nekonečno >
.      | < 0; 1 >
+	   | < 1; nekonečno >
{n}	   | presne n zhoda
{n,}   | < n; nekonečno >
{,m}   | <  ; m >
{n,m}  | < n; m >

* Zdroj: https://phoenixnap.com/kb/grep-regex#ftoc-heading-9