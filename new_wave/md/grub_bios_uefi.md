## <ins>GRUB/BIOS/UEFI</ins>

GRUB - GNU GRand Unified Bootloader, [link](https://en.wikipedia.org/wiki/GNU_GRUB)

NVRAM - non-volatile random-access memory

UEFI - Unified Extensible Firmware Interface, [link](https://en.wikipedia.org/wiki/UEFI)
<br>
<br>

#### 1. Linux shell utilities:

* `efibootmgr` - is a userspace application used to modify the UEFI Boot Manager. Popisuje "boot order" postupnosť.

* `os-prober` - utilita na detekciu všetkých operačných systémov na všetkých diskoch.V GRUB ju treba povoliť(súbor `/etc/default/grub`):

		:::bash
		GRUB_DISABLE_OS_PROBER=false

* `grub-install` - nainštaluje GRUB na DISK(**nikdy sa toto nerobí len pre partíciu**):

		:::bash
		grub-install -v /dev/sda

* `update-grub` - aktualizácia grub konfigurácie v súbore `/boot/grub/grub.cfg` so všetkými boot partitions ktoré nájde na všetkých pripojených diskoch a partíciách.

#### 2. Príkazy v konzole GRUB:

* `ls` - zobrazí zoznam všetkých pripojených jednotiek(partitions)

* `configfile` - načítanie konfiguračného súboru rovno z GRUB konzoly, príklad:

		:::bash
		grub> configfile (hd0,msdos2)/boot/grub/grub.cfg

Príklad postupnosti príkazov z konzoly GRUB na nabootovanie partície:

	:::bash
	grub> linux (hd0,gpt2)/vmlinuz root=/dev/sda2
	grub> initrd (hd0,gpt2)/initrd.img
	grub> boot

[Zdroj ROOT.CZ](https://www.root.cz/clanky/jak-funguje-a-jak-nastavit-bootovani-aneb-grub-1-2-mbr-uefi/)
