## <ins>inxi</ins>

### 1. Základy

Informácia o pamäťových moduloch: typ, rýchlosť, veľkosť v GB:

	:::bash
    sudo inxi -m

Informácia o sieťových adaptéroch(network drivers):

	:::bash
    inxi -n

Informácia o repositároch:

	:::bash
    inxi -r

Komplet informácia o zariadení, aj kernel driver, teploty, verzie...:

	:::bash
    inxi -Fz