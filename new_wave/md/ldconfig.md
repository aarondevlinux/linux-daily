## <ins>ldconfig - configure dynamic linker run-time bindings</ins>

K ldconfig patria aj `LD_LIBRARY_PATH`, `LD_PRELOAD` a príkaz `ldd`.

ldconfig command is used to tell the system about new locations of shared libraries.<br>
The uses information provided by the **/etc/ld.so.conf** configuration file.

The ldconfig command creates a cache database of all libraries based on the configuration file. This cache is normally stored in the /etc/ld.so.cache file. 

### 1. Hľadanie
Pohľadá dynamickú knižnicu lokálne, nie package v repozitároch:

	:::bash
	ldconfig -p | grep libpython3

Zdroj: [thegeekdiary](https://www.thegeekdiary.com/how-to-use-ldconfig-command-in-linux/)