## <ins>Linux - compress/decompress</ins>

### 1. Compressing

* základný

		:::bash
		tar -czvf output.tar.gz inputfileorfolder

* compress s vynechaním, tu vynechá všetky dir/file začínajúce bodkou:

		:::bash
		tar -czvf output.tar.gz --exclude ".*" input