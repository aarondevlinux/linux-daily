## <ins>Linux - firewall</ins>

### 1. Deny

	:::bash
	iptables -A INPUT -s <ip> -j DROP

### 2. Allow

Povolí pristup na local port `8000` z external IP `172.17.0.2`:

	:::bash
	sudo ufw allow from 172.17.0.2 to any port 8000

### 3. Rules

Zobrazenie user rules s poradovými číslami:

	:::bash
	sudo ufw status numbered

Zmazanie rule:

	:::bash
	sudo ufw delete 4
