## <ins>Linux init system</ins>

### 1. Základy

- init system je process v každom Linux s PID=1
- existujú viaceré init systémy: systemd, OpenRC, SysV, s6
- ako overiť aký je použitý: `sudo /sbin/init --version`

### 2. Príklady
Povolenie firewall(ufw) pre init system:

* OpenRC(príklad `Artix Linux`):
    
        :::bash
        sudo rc-update add ufw default

* systemd:

        :::bash
        sudo systemctl enable ufw

    alebo pridať start do startup súboru `sudo vim /etc/rc.local` nasledovne:

        :::bash
        ufw enable

<br><br>
Zdroj: [tecmint](https://www.tecmint.com/best-linux-init-systems/)