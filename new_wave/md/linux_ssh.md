## <ins>SSH</ins>

### 1. Generovanie a pridanie kľúča na remote server

* generovanie lokálneho kľúča:

        :::bash
        ssh-keygen

* zaregistrovanie kľúča na remote host:

        :::bash
        ssh-copy-id username@remote_host
