## <ins>Makefile</ins>

### 1. Jednoduchý príklad

Tento makefile skompiluje všetky *.cpp súbory v aktuálnom a sub adresároch.<br>Výsledná binárka bude `makefile_executable`

	:::basemake
	#https://avikdas.com/2019/12/16/makefiles-for-c-cpp-projects.html
	EXT_LIB_FOLDER=ext/lib
	EXT_INCLUDE_FOLDER=ext/include

	SRC_FOLDER = .
	OBJ_FOLDER = obj

	CPPFILES = $(shell find $(SRC_FOLDER) -name '*.cpp' ! -path './excluded.cpp')
	OBJFILES = $(patsubst $(SRC_FOLDER)/%.cpp,$(OBJ_FOLDER)/%.o,$(CPPFILES))
	OUT      = makefile_executable

	CXXFLAGS = -Wall -I./include -I${EXT_INCLUDE_FOLDER} -std=c++20 -g
	LDLIBS   = -L${EXT_LIB_FOLDER}/open62541 -lopen62541
	LDLIBS  += -L$(EXT_LIB_FOLDER)/tomlcpp -ltomlcpp
	LDLIBS  += -laws-cpp-sdk-dynamodb -laws-cpp-sdk-core

	$(OUT): $(OBJFILES)
		$(CXX) $(CXXFLAGS) -o $(OUT) $(OBJFILES) $(LDLIBS)

	$(OBJ_FOLDER)/%.o: $(SRC_FOLDER)/%.cpp
		@mkdir -p $(@D)
		$(CXX) $(CXXFLAGS) -c $< -o $@

	.PHONY: clean
	clean:
		rm -rf $(OBJ_FOLDER) $(OUT)


### 2. Internal makefile variables

Variable | Popis
-------- | -------------
CC       | Program for compiling C programs; default ‘cc’
CXX      | Program for compiling C++ programs; default ‘g++’
CXXFLAGS | Extra flags to give to the C++ compiler

<br><br>
Zdroj: [gnu.org](https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html)
