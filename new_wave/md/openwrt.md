## <ins>OpenWrt</ins>

Upgrade všetkých packages(LEN V AKÚTNYCH PRÍPADOCH!):

	:::bash
	opkg list-upgradable | cut -f 1 -d ' ' | xargs -r opkg upgrade

Zmena opkg konfigurácie balíčka, link:

	:::bash
	opkg install diffutils

	find /etc -name *-opkg # locate all -opkg files

	diff /etc/config/snmpd /etc/config/snmpd-opkg # Príklad pre 'snmpd' package

Ako uvoľniť miesto v RAM/storage:

	:::bash
	free # Zobraziť informáciu o RAM

	rm -r /tmp/opkg-lists/

	sync && echo 3 > /proc/sys/vm/drop_caches
