## <ins>pacman - package manager utility</ins>

### 1. Hľadanie
Pohľadá knižnicu v repozitároch(funguje len online => hľadá v remote repo)

    :::pacmanconf
    sudo pacman -Fy && pacman -Fx libalsa*

Závislosti pre package, tu 'git' - môže nastať situácia, že chýba určitá dependency pre package.<br>
V prípade git môže chýbať package, ktorý spôsobí error `wish not found`<br>
V tomto prípade by mal chýbať package `tk` => `sudo pacman -Sy tk`

    :::bash
    pacman -Ql git | grep gitk

### 2. Keyrings
Po offline inštalácii ak nefunguje repo update:

    :::bash
    sudo pacman -S archlinux-keyring
	sudo pacman-key --refresh-keys


### 3. Remove

Odstránenie orphaned packages:

    :::bash
    sudo pacman -Qdtq | sudo pacman -Rns -

Odstránenie konkrétneho package:

    :::bash
    sudo pacman -R pkg


Zdroj: https://www.jr-it-services.de/title-a-guide-to-cleaning-up-build-artifacts-on-arch-linux-for-a-leaner-system/