## <ins>snap</ins>

### 1. Príklad inštalácie:

	:::bash
	sudo snap install red-app

Nainštalované aplikácie sú v adresári: `/snap/bin`
