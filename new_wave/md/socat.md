## <ins>socat</ins>

- počúva na localhost:443, nezasahuje do komunikácie(iba log). Všetko čo príde vypíše do súboru '/tmp/test.log':

		:::bash
		sudo socat -u TCP4-LISTEN:443,reuseaddr,fork OPEN:/tmp/test.log,creat,append


- lokálny echo server na porte 1234:

		:::bash
		socat -v tcp-l:1234,fork exec:'/bin/cat'