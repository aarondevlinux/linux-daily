## <ins>tcpdump</ins>

### 1. Monitoring

* na porte 443

		:::bash
		sudo tcpdump -i any port 443

* pre host 192.168.0.1

		:::bash
		sudo tcpdump host 192.168.0.1