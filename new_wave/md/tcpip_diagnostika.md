## <ins>TCP/IP diagnostika</ins>

#### 1. DNS

Zobrazenie použitého DNS:

	:::bash
	cat /etc/resolv.conf

ALEBO

	:::bash
	nmcli dev show | grep 'DNS'


#### 2. MTU - maximum packet size that can be transmitted over your network

	:::bash
	ip a | grep mtu

#### 3. Interface up/down

	:::bash
	sudo ifconfig <ifname> down

ALEBO

	:::bash
	sudo ifup <ifname>
