## <ins>tcpkill</ins>

### 1. Blocking

* zablokuje komunikáciu pre IP adresu:

		:::bash
		sudo tcpkill -9 -i wlp0s20f3 host google.com
