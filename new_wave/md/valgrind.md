## <ins>valgrind</ins>

### 1. Jednoduchý príklad:

	:::cpp
	#include <stdio.h>
	#include <stdlib.h>

	void memory_leak_example() {
		int* memory_leak = malloc(sizeof(int) * 10);
		//Chýba free() pre "int* memory_leak"
	}

	int main() {
		//printf("Valgrind test.\n");
		memory_leak_example();
		return 0;
	}

Použitie valgrind na predchádzajúci kód:

	:::bash
	valgrind --leak-check=full ./memory_leak_example
