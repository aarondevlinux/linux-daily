## <ins>vi editor</ins>

### 1. Editor commands

Kláves | Popis
------ | -------------
:q     | ukončenie editoru
:q!    | ukončenie editoru bez uloženia

### 2. Vkladanie a modifikácia textu

Kláves | Popis
------ | ----------
i      | text bude vkladaný od aktuálnej pozície kurzoru
R      | replace - prepisovanie znakov od aktuálnej pozície
C      | to isté ako R, ale zmaže všetko po koniec riadku od aktuálnej pozície(po stlačení Esc)
S      | zmazanie aktuálneho riadku + edit od prvého znaku v riadku