## <ins>Wireshark</ins>

### 1. Nejaké príklady filtrov:

	:::bash
	ip.dst == 192.0.2.1

	ip.src == 192.0.2.1

	frame.len > 100

	tcp.flags.syn == 1

	http.cookie contains "sessionId"