#bs4 docu: https://www.crummy.com/software/BeautifulSoup/bs4/doc/#navigablestring-and-new-tag
from bs4 import BeautifulSoup as Soup
import os

path = "./html"
placeholderHtml = ""

#loop 
for filename in os.listdir(path=path):
	
	#loop vsetky nagenerovane HTML z markdown(awk, bash, ...)
	if filename.endswith(".html"):
		subor = f"{path}/{filename}"
		print(f"Ideme subor: {subor}" )

		with open(subor,"r") as f:
			currentHtmlSubor = f.read()
			currentHtml = "<div class='jednaTemaObsahu'"
			if placeholderHtml != "":
				currentHtml += " hidden" #HIDDEN(len bash, prva tema, bude viditelna po reload/init stranky)

			currentHtml = currentHtml + " idTemy='" + filename + "'>" + currentHtmlSubor + "</div>"
			#print( currentHtml )
			#exit(0)

			#parse
			soup = Soup(currentHtml, features="html.parser")
			nazovTemy = soup.find("h2").string
			nazovTemy = nazovTemy.lower()
			print("  - tema obsahu:", nazovTemy )
		
			#loop vsetky zdrojove kody v HTML(div class='codehilite')
			srcCounter = 0

			for item in soup.findAll("div", {"class": "codehilite"}, partial=False):

				srcCounter += 1

				htmlSrcItemId = f'{nazovTemy}{srcCounter}'.replace(' ', '_').replace('/', '_').replace('-','minus') #jednoznacny identifikator jendeho div pre zdrojovy kod(<code>)
				htmlSrcItemId = htmlSrcItemId.replace('á', 'a').replace('í', 'i').replace('é', 'e') #nestandardne znaky
				htmlSrcItemId = htmlSrcItemId.replace('+', 'plus').replace('-', 'minus').replace('/', 'forwardslash') # +,-./...
				htmlSrcItemId = htmlSrcItemId.replace('.','dot')
				print(f"    -> spracovanie zdrojoveho kodu: {srcCounter}, id: {htmlSrcItemId}")

				divs = item #soup.find("div", {"class": "codehilite"}, partial=False)
				if divs == None:
					print("Ziaden div so source code..toto je asi chyba....");
					exit(0)

				#div: container - top level
				container_div = soup.new_tag("div")
				#container_div["hidden"] = "hidden"
				container_div["class"] = "container"
				#container_div = Soup('<div class=\'container\' hidden></div>', 'html.parser')
				#div: collapse
				collapse_div = soup.new_tag("div")
				collapse_div["id"] = htmlSrcItemId
				collapse_div["class"] = "collapse in"

				#button: +/-
				buttonCollapse = soup.new_tag("button")
				buttonCollapse["type"] = "button"
				buttonCollapse["style"] = "display: inline;"
				buttonCollapse["class"] = "btn btn-info"
				buttonCollapse["data-toggle"] = "collapse"
				buttonCollapse["data-target"] = "#" + htmlSrcItemId
				buttonCollapse.string = "+/-"
				container_div.insert(0, buttonCollapse)

				#button: copy clipboard
				buttonClp = soup.new_tag("button")
				clpButtonId = "btnclipboard" + htmlSrcItemId
				buttonClp["id"] = clpButtonId
				buttonClp["class"] = "clsClipboardButton"
				buttonClp["style"] = "display:inline-block; float: right; background-color: Transparent;"
				#print("Mame zdrojak z MD:", divs.text)
				buttonClp["onclick"] = "copyToClipBoard('"+ htmlSrcItemId + "'," + clpButtonId + ")" # parametre: div pre zdrojak + copy button(zahada, sem posielam id, ale do funkcie pride uz hotovy button)
				buttonClp.string = "Copy"
				container_div.insert(1, buttonClp)

				divs.wrap(collapse_div)
				collapse_div.wrap(container_div)

			#Makefile potrebuje tabs(\t)
			if filename == "makefile.html":
				print("  - nahradzanie 4 medzier tabulatorom...") 
				finalSoup = str(soup).replace('<span class="w">    </span>', '<span class="w">&#9;</span>')
			else:
				finalSoup = str(soup)
			placeholderHtml += finalSoup #Soup docu link: https://beautiful-soup-4.readthedocs.io/en/latest/#output

#vytvorenie finalneho index.html
with open("template.html", "rt") as fin:
	templateHtmlString = fin.read()

	with open("index.html", "wt") as fout:
		indexHtmlString = templateHtmlString.replace('^^PLACEHOLDER^^', placeholderHtml )
		fout.write(indexHtmlString)
	
	#print(placeholderHtml)

exit(0)

'''
<pre class="codehilite"><code class="language-bash">man test
</code></pre>
'''
'''
<div class="container" >
<button class="btn btn-info" data-target="#bash1" data-toggle="collapse" style="display: inline;" type="button">
  +/-
 </button>
 <button id="uvidimeAjClpRaz" style="display:inline-block; float: right;">
  Copy
 </button>
<div class="collapse in" id="bash1">
 <pre class="codehilite"><code class="language-bash">man test
</code></pre>
</div>
</div>
'''

'''copyButtonHtml = '<button style="display:inline-block; float: right;" onclick="copyToClickBoard()" >Copy</button>'
sourceCodeDivId = 'bash1'
collapseButtonHtml = '<button type="button" style="display: inline;" class="btn btn-info" data-toggle="collapse" data-target="#'+ sourceCodeDivId +'">+/-</button></p>'
sourceCodeDivHtml = '<div id="'+ sourceCodeDivId +'" class="collapse in">'
'''