#!/usr/bin/bash
#pip install markdown Pygments

for filename in md/*; do
	fileNameOnly=$(basename ${filename%.*})
	#echo "=> filename relative='${filename}'; fileNameOnly='${fileNameOnly}'"
	python3 -m markdown -x codehilite -x tables md/${fileNameOnly}.md > html/${fileNameOnly}.html
done

#Render vyrobenych HTML(pridanie copy to clipboard button-u, ...)
python3 ./render_html.py
exit 0

#Vyroba HTML
python3 -m markdown -x codehilite md/bash.md > html/bash.html
python3 -m markdown -x codehilite md/awk.md > html/awk.html
python3 -m markdown -x codehilite md/pacman.md > html/pacman.html
python3 -m markdown -x codehilite md/inxi.md > html/inxi.html
python3 -m markdown -x codehilite md/git_commit.md > html/git_commit.html
python3 -m markdown -x codehilite md/makefile.md > html/makefile.html
#Render vyrobenych HTML(pridanie copy to clipboard button-u, ...)
python3 ./render_html.py
